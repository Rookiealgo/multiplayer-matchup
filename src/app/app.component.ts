import { Component, Input, Output, EventEmitter } from '@angular/core';
import {Observable} from 'rxjs/Observable';

import { SwitchPageService } from './services/switchPage.component';
import { LoginService } from './services/login.component';

import { CoolSessionStorage } from 'angular2-cool-storage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [
    SwitchPageService,
    LoginService
  ],
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  sessionStorage: CoolSessionStorage;
  loggedin : boolean;
  @Output() haslogin: EventEmitter<boolean> = new EventEmitter<boolean>();
  subscription: any;
  constructor(
    private switchPageService: SwitchPageService,
    private loginService: LoginService,
    sessionStorage: CoolSessionStorage,
  ) {
    this.sessionStorage = sessionStorage;
    let token = this.sessionStorage.getItem('token');
    if (token == 'test-token')
      this.loggedin = true;
    else
      this.loggedin = false;
  }
  onNotify(login:boolean):void {
    this.loggedin = login;
    this.haslogin.emit(this.loggedin);
    this.switchPageService.switchToMainPage();
    this.loginService.login.emit(this.loggedin);
  }
}
