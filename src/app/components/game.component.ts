import { Component, Input, Output } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { CoolSessionStorage } from 'angular2-cool-storage';

@Component({
  selector: 'game',
  templateUrl: 'game.component.html',
  providers: [
  ],
  styleUrls: ['../app.component.css']
})
export class GameComponent {
  sessionStorage: CoolSessionStorage;
  loggedin: boolean;
  Game: any;
  constructor(
    private activatedRoute: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
        let name = params['name'];
        // get data from database
        // validate login
        this.loggedin = true;
        this.Game = {
          'name': name,
          'src': "/app/static/img/league-of-legends.jpg",
          'description': "League of Legends (abbreviated LoL) is a multiplayer online battle arena video game developed and published by Riot Games for Microsoft Windows and macOS. The game follows a freemium model and is supported by microtransactions, and was inspired by the Warcraft III: The Frozen Throne mod, Defense of the Ancients. In League of Legends, players assume the role of an unseen \"summoner\" that controls a \"champion\" with unique abilities and battle against a team of other players or computer-controlled champions. The goal is usually to destroy the opposing team's \"nexus\", a structure which lies at the heart of a base protected by defensive structures, although other distinct game modes exist as well. Each League of Legends match is discrete, with all champions starting off fairly weak but increasing in strength by accumulating items and experience over the course of the game. The champions and setting blend a variety of elements, including high fantasy, steampunk, folklore, and Lovecraftian horror."
        }
        console.log(name);
      });
  }

}
