import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

import {FacebookService, FacebookInitParams, FacebookLoginResponse} from 'ng2-facebook-sdk';

import { CoolSessionStorage } from 'angular2-cool-storage';

declare const FB:any;

@Component({
  selector: 'social-auth',
  templateUrl: './socialAuth.component.html',
  providers: [
    FacebookService,
  ],
  styleUrls: ['../app.component.css']
})

export class socialAuth {

  @Input() loggedin:boolean;
  username: string;
  password: string;
  sessionStorage: CoolSessionStorage;
  @Output() notify: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    sessionStorage: CoolSessionStorage,
    private fb: FacebookService,
  )
  {
    this.sessionStorage = sessionStorage;
    let fbParams: FacebookInitParams = {
       appId: '1106155602787860',
       xfbml: true,
       version: 'v2.7'
    };
    this.fb.init(fbParams);
  }

  login() {
    // pass username and password
    // generate a token and save in backend
    // save the token to browser
    this.sessionStorage.setItem('token','test-token');
    this.notify.emit(true);
  }

  FBlogin() {
    this.fb.login().then(
    (response: FacebookLoginResponse) => {
      //connect to backend to auth
      //generate a token and save in backend
      //save the token to browser
      this.notify.emit(true);
      this.sessionStorage.setItem('token','test-token');
      console.log(response['authResponse']);
      this.fb.logout().then(
        (response: FacebookLoginResponse) => {
          console.log(response['authResponse'])
        },
        (error: any) => console.error(error));
    },
    (error: any) => console.error(error));
  }

  logout() {
    // remove the token from browser
    this.sessionStorage.removeItem('token');
    this.notify.emit(false);
  }

  showLogin() {

  }

  showRegist() {

  }
}
