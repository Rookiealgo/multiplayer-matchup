import { Component, Input, Output } from '@angular/core';
import { SwitchPageService } from '../services/switchPage.component';
import { CoolSessionStorage } from 'angular2-cool-storage';

import { LoginService } from '../services/login.component';


@Component({
  selector: 'content',
  templateUrl: './content.component.html',
  providers: [
    LoginService,
    SwitchPageService,
  ],
  styleUrls: ['../app.component.css']
})
export class ContentComponent {
  sessionStorage: CoolSessionStorage;

  loggedin: boolean;
  z = 0;
  x = 1;
  subscription: any;

  GameList = [
    [
      {"name":"League of Legends","type": "Multiplayer Online Battle Arena" ,"src":"/app/static/img/league-of-legends.jpg"},
      {"name":"Overwatch" ,"type": "Multiplayer First-person Shooter" ,"src":"/app/static/img/overwatch.png"},
      {"name":"Minecraft" ,"type": "Sandbox" ,"src":"/app/static/img/minechaft.jpg"}
    ],
    [
      {"name":"Battlefield 4" ,"type": "First-person Shooter" ,"src":"/app/static/img/battlefield.jpg"},
      {"name":"World of WarCraft" ,"type": "Massively Multiplayer Online RPG" , "src":"/app/static/img/World of WarCraft.jpg"},
      {"name":"Hearthstone", "type":"online collectible card","src":"/app/static/img/hearthstone.jpg"}
    ],
    [
      {"name":"Starcraft", "type": "Real Time Strategy", "src":"/app/static/img/starcraft.jpg"},
      {"name":"FIFA 17", "type": "Association Football Simulation", "src":"/app/static/img/fifa17.jpg"},
      {"name":"Dota 2", "type": "Multiplayer Online Battle Arena", "src":"/app/static/img/data 2.jpg"}
    ]
  ];

  constructor(
    private switchPageService: SwitchPageService,
    private loginService: LoginService,
  ) {
    this.loggedin = true;
    // get game list from database
  }



  getGameInfo(name) {

    this.switchPageService.switchToGame(name);
  }

}
