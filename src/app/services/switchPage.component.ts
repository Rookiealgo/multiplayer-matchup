import { Injectable } from '@angular/core';
import {  Router, NavigationExtras } from '@angular/router';

@Injectable()
export class SwitchPageService {

  constructor(private router:Router){}

  switchToMainPage() {
    this.router.navigate(['']);
  }

  switchToGame(game) {
    this.router.navigate(['game'],  {queryParams: {'name': game}});
  }

}
