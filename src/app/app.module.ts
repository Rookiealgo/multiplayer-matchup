import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CoolSessionStorage } from 'angular2-cool-storage';

import { socialAuth } from './components/socialAuth.component';
import { ContentComponent } from './components/content.component';
import { GameComponent } from './components/game.component';

import { LoginService } from './services/login.component';

const appRoutes: Routes = [
  { path: '', component: ContentComponent },
  { path: 'game', component: GameComponent },
  // { path: 'game', component: GameComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    socialAuth,
    GameComponent,
    ContentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    CoolSessionStorage,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
