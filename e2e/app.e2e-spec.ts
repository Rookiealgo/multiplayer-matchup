import { MultiplayerMatchupPage } from './app.po';

describe('multiplayer-matchup App', function() {
  let page: MultiplayerMatchupPage;

  beforeEach(() => {
    page = new MultiplayerMatchupPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
